#include "Products.h"
#include<iostream>
#include <string>

Product::Product(uint16_t id, const std::string& name, float price, uint8_t vat, const std::string& dateOrType) :
	m_id(id), m_name(name), m_price(price), m_vat(vat), m_dateOrType(dateOrType)
{
}

uint8_t Product::getVat()
{
	return m_vat;
}

float Product::getPrice()
{
	return m_price;
}

bool Product::operator < (const Product& product) const
{
	return (m_price < product.m_price);
}

bool Product::operator<=(const Product& product) const
{
	return (m_name <= product.m_name);
}

std::ostream& operator<<(std::ostream& out, Product& product)
{
	out << "ID = " << product.m_id << std::endl;
	out << "NAME = " << product.m_name<< std::endl;
	out << "PRICE = " << product.m_price << std::endl;
	out << "VAT = " << unsigned(product.m_vat) << std::endl;
	out << "DATE OR TYPE = " << product.m_dateOrType << std::endl;
	return out;
}
